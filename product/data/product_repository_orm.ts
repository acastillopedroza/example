import { Repository } from "typeorm"
import { Product } from "../application/product"
import { ProductRepository } from "../application/product_repository"
import { ProductOrm } from "./product_orm"

export class ProductRepositoryOrm implements ProductRepository {
    constructor(private productRepository: Repository<ProductOrm>) { }

    async register(product: Product): Promise<void> {
        const productOrm = toProductOrm(product)
        await this.productRepository.save(productOrm)
    }

    async fetch(): Promise<Product[]> {
        const productsOrm = await this.productRepository.find()
        return productsOrm.map((productOrm) => new Product(productOrm.uuid, productOrm.name, productOrm.description, productOrm.price))
    }

    async findByUuid(uuid: string): Promise<Product> {
        const productOrm = await this.productRepository.findOneBy({ uuid: uuid })
        return toProduct(productOrm)
    }

    async update(product: Product) {
        const productOrm = await this.productRepository.findOneBy({ uuid: product.uuid })
        productOrm.name = product.name
        productOrm.price = product.price
        await this.productRepository.save(productOrm)
    }

    async delete(uuid: string) {
        const productOrm = await this.productRepository.findOneBy({ uuid: uuid })
        await this.productRepository.delete(productOrm)
    }
}

const toProductOrm = (product: Product): ProductOrm => {
    const productOrm = new ProductOrm();
    productOrm.uuid = product.uuid
    productOrm.name = product.name
    productOrm.description = product.description
    productOrm.price = product.price

    return productOrm
}

const toProduct = (productOrm: ProductOrm): Product => {
    return new Product(
        productOrm.uuid,
        productOrm.name,
        productOrm.description,
        productOrm.price,
    )
}
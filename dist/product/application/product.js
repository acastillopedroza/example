"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
var Product = /** @class */ (function () {
    function Product(uuid, name, description, price) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.price = price;
    }
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map
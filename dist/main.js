"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var data_source_1 = require("./product/data/data_source");
var router_1 = require("./product/router");
var app = (0, express_1.default)();
app.use(express_1.default.json());
app.use("/api/v1/products", router_1.productRouter);
app.listen(9000, function () {
    console.log("[server]: Server running listen 9000");
});
data_source_1.appDataSource.initialize()
    .then(function () {
    console.log("[db]: Data base is running successful");
})
    .catch(function (error) { return console.log(error); });
//# sourceMappingURL=main.js.map
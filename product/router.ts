import express, { Express, Request, Response } from "express"
import { Product } from "./application/product";
import { appDataSource } from "./data/data_source";
import { ProductOrm } from "./data/product_orm";
import { ProductRepositoryOrm } from "./data/product_repository_orm";

export const productRouter = express.Router()
    .post("/", async (request: Request, response: Response) => {
        const { body } = request
        const productRepository = appDataSource.getRepository(ProductOrm)
        const productRepositoryOrm = new ProductRepositoryOrm(productRepository)
        await productRepositoryOrm.register(new Product(
            body.uuid,
            body.name,
            body.description,
            body.price,
        ))
        response.send("Product registered successful")
    })
    .get("/", async (request: Request, response: Response) => {
        const productRepository = appDataSource.getRepository(ProductOrm)
        const productRepositoryOrm = new ProductRepositoryOrm(productRepository)
        const products = await productRepositoryOrm.fetch()
        response.send(products)
    })
    .get("/:productId", async (request: Request, response: Response) => {
        const {
            params: { productId },
        } = request;
        const productRepository = appDataSource.getRepository(ProductOrm)
        const productRepositoryOrm = new ProductRepositoryOrm(productRepository)
        const products = await productRepositoryOrm.findByUuid(productId)
        response.send(products)
    })
    .put("/:productId", async (request: Request, response: Response) => {
        const { body } = request
        const productRepository = appDataSource.getRepository(ProductOrm)
        const productRepositoryOrm = new ProductRepositoryOrm(productRepository)
        await productRepositoryOrm.update(new Product(
            body.uuid,
            body.name,
            body.description,
            body.price,
        ))
        response.send("Product updated successful")
    })
    .delete("/:productId", async (request: Request, response: Response) => {
        const {
            params: { productId },
        } = request;
        const productRepository = appDataSource.getRepository(ProductOrm)
        const productRepositoryOrm = new ProductRepositoryOrm(productRepository)
        await productRepositoryOrm.delete(productId)
        response.send("Product deleted successful")
    });

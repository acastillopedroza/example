import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class ProductOrm {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    uuid: string

    @Column()
    name: string

    @Column()
    description: string

    @Column("double")
    price: number
}
import { DataSource } from "typeorm";
import { ProductOrm } from "./product_orm";

export const appDataSource = new DataSource({
    type: "mysql",
    host: "example.cbhtvpmi421p.us-east-1.rds.amazonaws.com",
    port: 3306,
    database: "example",
    username: "root",
    password: "Example123456",
    synchronize: true,
    logging: true,
    entities: [ProductOrm],
    subscribers: [],
    migrations: [],
})


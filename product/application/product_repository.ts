import { Product } from "./product"

export interface ProductRepository {
    register(product: Product): Promise<void>
    fetch(): Promise<Product[]>
    findByUuid(uuid: string): Promise<Product>
    update(product: Product): Promise<void>
    delete(uuid: string): Promise<void>
}


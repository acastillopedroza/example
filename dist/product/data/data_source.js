"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appDataSource = void 0;
var typeorm_1 = require("typeorm");
var product_orm_1 = require("./product_orm");
exports.appDataSource = new typeorm_1.DataSource({
    type: "mysql",
    host: "example.cbhtvpmi421p.us-east-1.rds.amazonaws.com",
    port: 3306,
    database: "example",
    username: "root",
    password: "Example123456",
    synchronize: true,
    logging: true,
    entities: [product_orm_1.ProductOrm],
    subscribers: [],
    migrations: [],
});
//# sourceMappingURL=data_source.js.map
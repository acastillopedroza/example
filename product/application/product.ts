export class Product {
    constructor(
        public uuid: string,
        public name: string,
        public description: string,
        public price: number,
    ) { }
}
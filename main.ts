
import express, { Express } from "express"
import { appDataSource } from "./product/data/data_source"
import { productRouter } from "./product/router";

const app = express()

app.use(express.json());

app.use("/api/v1/products", productRouter);

app.listen(9000, () => {
    console.log("[server]: Server running listen 9000")
})

appDataSource.initialize()
    .then(() => {
        console.log("[db]: Data base is running successful")
    })
    .catch((error) => console.log(error))